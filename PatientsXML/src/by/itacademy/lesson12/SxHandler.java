package by.itacademy.lesson12;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class SxHandler extends DefaultHandler {

    private boolean surnameFlag = false;
    private boolean birthDayFlag = false;
    private boolean illnessFlag = false;
    private String name, surname;
    private LocalDate birthDay;
    private boolean illness;
    private Map<Patient, Boolean> outPutMap = new HashMap<Patient, Boolean>();

    public Map<Patient, Boolean> getOutPutMap() {
        return outPutMap;
    }

    public void setEmptyMap() {
        outPutMap.clear();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("patient")) {
            name = attributes.getValue("name");
        } else if (qName.equalsIgnoreCase("surname")) {
            surnameFlag = true;
        } else if (qName.equalsIgnoreCase("birthDay")) {
            birthDayFlag = true;
        } else if (qName.equalsIgnoreCase("illness")) {
            illnessFlag = true;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (surnameFlag) {
            surname = new String(ch, start, length);
            surnameFlag = false;
        } else if (birthDayFlag) {
            birthDay = LocalDate.parse((new String(ch, start, length)), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
            birthDayFlag = false;
        } else if (illnessFlag) {
            illness = Boolean.getBoolean(new String(ch, start, length));
            illnessFlag = false;
            Patient patient = new Patient(name, surname, birthDay, illness);
            outPutMap.put(patient, illness);
            illnessFlag = false;
        }
    }
}

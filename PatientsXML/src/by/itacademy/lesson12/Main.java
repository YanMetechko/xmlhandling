package by.itacademy.lesson12;

import java.io.File;
import java.time.LocalDate;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        XMLProcessor processor = new XMLProcessor();
        File file = new File("Patients.xml");
        Map<Patient, Boolean> map = processor.readerDOM(file);
        processor.addItem(map, new Patient("Vasja","Chylkov", LocalDate.of(1983,5,27),true));
        file = new File ("PatientsDom.xml");
        processor.fileDOMUpdate(map, file);
        map = processor.readerSAX(file);
        processor.fileSTAXUpdate(map, new File("patientsSTAX.xml"));
    }
}

package by.itacademy.lesson12;

import java.time.LocalDate;
import java.util.Objects;

public class Patient {
    private String name;
    private String surname;
    private LocalDate birthDay;
    private boolean illness;

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public Patient(String name, String surname, LocalDate birthDay, boolean illness) {
        this.name = name;
        this.surname = surname;
        this.birthDay = birthDay;
        this.illness = illness;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name,surname,birthDay);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;
        Patient patient = (Patient) o;
        return Objects.equals(name, patient.name) && Objects.equals(surname, patient.surname) && Objects.equals(birthDay, patient.birthDay);
    }

    public boolean illOrNot() {
        return illness;
    }
}

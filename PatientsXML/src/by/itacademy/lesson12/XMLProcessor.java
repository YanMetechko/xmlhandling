package by.itacademy.lesson12;


import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;


public class XMLProcessor {
    private static final Logger LOGGER = Logger.getLogger(XMLProcessor.class.getName());

    public Map<Patient, Boolean> readerDOM(File file) {
        Map<Patient, Boolean> tempList = new HashMap<>();
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document dc = db.parse(file);
            Element root = dc.getDocumentElement();
            NodeList patients = dc.getElementsByTagName("Patient");
            for (int i = 0; i < patients.getLength(); i++) {
                Node patientNod = patients.item(i);
                if (patientNod.getNodeType() == Node.ELEMENT_NODE) {
                    Element ePatient = (Element) patientNod;
                    String name = ePatient.getAttribute("name");
                    String surname = ePatient.getElementsByTagName("surname").item(0).getTextContent();
                    LocalDate birthDay = LocalDate.parse(ePatient.getElementsByTagName("birthday")
                            .item(0).getTextContent(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
                    boolean isIll = Boolean.getBoolean(ePatient.getElementsByTagName("illness")
                            .item(0).getTextContent());
                    tempList.put(new Patient(name, surname, birthDay, isIll), isIll);
                }
            }
        } catch (ParserConfigurationException | IOException | SAXException e) {
            LOGGER.log(Level.INFO, e.getMessage());

        }
        return tempList;
    }

    public Map<Patient, Boolean> readerSAX(File file) {
        Map<Patient, Boolean> tempList = new HashMap<>();
        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser spr = spf.newSAXParser();
            SxHandler handler = new SxHandler();
            spr.parse(file, handler);
            tempList = handler.getOutPutMap();
        } catch (ParserConfigurationException | IOException | SAXException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
        return tempList;
    }

    public void addItem(Map<Patient, Boolean> map, Patient patient) {
        map.put(patient, (Boolean) patient.illOrNot());
    }

    public void fileDOMUpdate(Map<Patient, Boolean> map, File file) {
        try {
            Set<Patient> set = map.keySet();
            Iterator<Patient> it = set.iterator();
            DocumentBuilderFactory db = DocumentBuilderFactory.newInstance();
            DocumentBuilder dbr = db.newDocumentBuilder();
            Document doc = dbr.newDocument();
            Element rootElement = doc.createElement("Patients");
            doc.appendChild(rootElement);
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");

            while (it.hasNext()) {
                Patient ptnt = it.next();
                Element patient = doc.createElement("Patient");
                rootElement.appendChild(patient);
                Attr attr = doc.createAttribute("name");
                attr.setValue(ptnt.getName());
                patient.setAttributeNode(attr);
                Element srn = doc.createElement("surname");
                srn.appendChild(doc.createTextNode(ptnt.getSurname()));
                patient.appendChild(srn);
                Element birthDay = doc.createElement("BirthDay");
                String date = dtf.format(ptnt.getBirthDay());
                birthDay.appendChild(doc.createTextNode(date));
                patient.appendChild(birthDay);
                Element ils = doc.createElement("illness");
                ils.appendChild(doc.createTextNode(Boolean.toString(ptnt.illOrNot())));
                patient.appendChild(ils);
            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(file);
            transformer.transform(source, result);
        } catch (ParserConfigurationException| TransformerException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

    public void fileSTAXUpdate(Map<Patient, Boolean> map, File file) {
        try {
            Set<Patient> set = map.keySet();
            Iterator<Patient> it = set.iterator();
            XMLOutputFactory opF = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = opF.createXMLStreamWriter(new FileWriter(file));
            writer.writeStartDocument("1.0");
            writer.writeStartElement("patients");
            while (it.hasNext()) {
                Patient ptnt = it.next();
                writer.writeStartElement("patient");
                writer.writeAttribute("name", ptnt.getName());
                writer.writeStartElement("surname");
                writer.writeCharacters(ptnt.getSurname());
                writer.writeEndElement();
                writer.writeStartElement("birthDay");
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
                writer.writeCharacters(dtf.format(ptnt.getBirthDay()));
                writer.writeEndElement();
                writer.writeStartElement("illness");
                writer.writeCharacters(Boolean.toString(ptnt.illOrNot()));
                writer.writeEndElement();
                writer.writeEndElement();
            }
            writer.writeEndElement();
            writer.writeEndDocument();
            writer.flush();
        } catch (IOException | XMLStreamException e) {
            LOGGER.log(Level.INFO, e.getMessage());
        }
    }

}
